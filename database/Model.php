<?php
require_once("DB.php");
require_once("Utils.php");
class Model{
    public  $_table_name;
    private $_pdo;
    private $_sql;
    private $_where;
    private $_limit;
    private $_offset;
    private $_or;
    private $_and;

    function __construct(){
         $this->_pdo=new DB();
    }
    public function insert($fieldsValuesArray){
            $keyValues=getKeyValuesArrays($fieldsValuesArray);
            $sql = "INSERT INTO ".$this->_table_name ." ". $keyValues['keys']. " values ".getQuestionsMarks() ;
            
            $stmt= $this->_pdo->prepare($sql);
       return      $stmt->execute($keyValues['values']);
    }
    public function update($fieldsValuesArray,$id){
 
        $sql = "UPDATE ".$this->_table_name." SET ".getPlaceHolders($fieldsValuesArray)." WHERE id=:id";
        $stmt= $this->_pdo->prepare($sql);
       return  $stmt->execute($fieldsValuesArray);
        }
    public function deleteByID($id){
        $sql = "DELETE FROM ".$this->_table_name." WHERE id=:id";
        $stmt= $this->_pdo->prepare($sql);
       return  $stmt->execute(['id'=>$id]);
    }

    public function findById($id){
// select a particular user by id
        $stmt = $this->_pdo->prepare("SELECT * FROM ". $this->_table_name."  WHERE id=:id");
        $stmt->execute(['id' => $id]); 
        return  $stmt->fetch();
    }
    public function getAll(){
        $DB=new DB();
        $this->_pdo=$DB->openConnection();
        $stmt = $this->_pdo->prepare("SELECT * FROM ". $this->_table_name);
        
        return  $stmt->fetch();
    }

    public function findWithLimit($limit){
        $stmt = $this->_pdo->prepare("SELECT * FROM ". $this->_table_name." limit $limit ");
       
        return  $stmt->fetch();
    }
    public function limit($limit){
        $this->_limit=$limit;

    }
    public function offset($offset){
        $this->_offset=$offset;
    }
    public function where($conidtions){

    }
}