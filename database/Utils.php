<?php

function getKeyValuesArrays($array){
    $keys=[];
    $values=[];
    foreach ($array as $key=>$value){
        $key[]=$key;
        $values[]=$value;
    }
    return ['keys'=>$keys,'values'=>$values];

}

function getPlaceHolders($array){
    $placeholders=[];
    
    foreach ($array as $key=>$value){
        if ($key!='id'){
            $placeholders[]=$key."=:".$key;
        }
    }
    return implode(", ",$placeholders);
}
function getQuestionsMarks($array){
    $i=count($array);
    $q=[];
    
     for($s=0;$s<i;$s++){
        $q[]="?";
    }
    return "(".implode(",",$q).")";
}