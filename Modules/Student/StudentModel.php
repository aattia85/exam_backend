<?php

class StudentModel extends Model{

    public $_table_name="students";

    private $fields=[
        'id',
        'first_name',
        'last_name',
        'birthday',
        'email',
        'cellphone',
        'city',
        'province',
        'address',
        'educational_phase',
    ];

   
    public function __construct(){
        parent::__construct();
        $this->_table_name ="students";
    }
    

}