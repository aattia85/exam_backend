<?php
 
require_once   'StudentModel.php';
class Student {
    private $model; 
    function __construct(){
        $this->model=new StudentModel(); 
    }
    function index(){
        return ($this->model->getAll()) ;
    }
    function getAll(){
        return $this->model->getAll();
    }
}